# Pile [Jamstack](https://jamstatic.fr/2019/02/07/c-est-quoi-la-jamstack/) pour la gestion du contenu éditorial du site [krad.xyz](https://krad.xyz)

basé sur [Hugo](https://gohugo.io/), [Netlify CMS](https://www.netlifycms.org) et [GitLab](https://gitlab.com).
