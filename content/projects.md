---
title: Projects
draft: false
description: Projects
press_kit_fr: files/arachnur2024-fr.pdf
press_kit_en: files/arachnur2024-en.pdf
technical_rider_fr: files/fiche-technique-arachnur-_cie-krad.pdf
technical_rider_en: files/technical-rider-arachnur_cie-krad.pdf
weight: 4
image: /img/leila-legs-1280.gif
menu: main
---
{{% section %}}

## *Project* **Arachnur**

{{% /section %}}

{{% section %}}

#### description

Cie kraD's first creation « Arachnur » is a fusion of contemporary circus and noise with a strong penchant towards the obscure.

In a universe of ropes and cables, the audience discovers a body caught under the weight of life, revealing its slow struggle towards a metaphorical suicide. It is a visceral dive into the abyss of the human soul through acoustical borderline and dark imagery. Some sort of macabre ritual, reflecting (on) despair and the harrowing loop of dark thoughts; an embodiment of a mental crawl towards the gallows.

« Arachnur » is beyond evil or good, definitely cathartic, but nevertheless poetic. An ongoing hymn to the dark and what lays within it, inspired by the quote of the painter Pierre Soulages: « Mon instrument n'était plus le noir, mais cette lumière secrète venue du noir. » « My instrument was no longer the dark, but that secret light that came from the dark. »

{{< vimeo 546922857 >}}

{{% /section %}}

{{% section %}}

#### Downloads

##### Français

{{< doc-button src="press_kit_fr" txt="Dossier de presse" >}}\
{{< doc-button src="technical_rider_fr" txt="Fiche technique" >}}

##### English

{{< doc-button src="press_kit_en" txt="Press kit" >}}\
{{< doc-button src="technical_rider_en" txt="Technical Rider" >}}
{{% /section %}}

{{% section %}}

#### Residencies

« Arachnur » is the first self-produced creation of Cie kraD. The first working sessions were held during January – April 2020 at Saules d'out in Geneva.

{{% greyed %}}
July 2020

##### Station Circus

Basel, July 2020,  (1 week)
{{% /greyed %}}

{{% greyed %}}
September 2020

##### CircusDanceFestival

Köln (1 week)
{{% /greyed %}}

{{% greyed %}}
December 2020 

##### cave12

Geneva (3 days – sound creation)
{{% /greyed %}}

{{% greyed %}}
April 2021

##### Le Grütli

Centre de production et de diffusion des Arts vivants, Geneva (1 week)
{{% /greyed %}}

{{% greyed %}}
September 2022

##### Comédie of Geneva (former)

(1 week)
{{% /greyed %}}

{{% greyed %}}
October 2022

##### Dampfzentrale

Bern (1 week), followed by the Premiere on the 15th of October 2022
{{% /greyed %}}
{{% /section %}}

{{% section %}}

#### Grants

{{% greyed %}}

##### Geneva city

grants for multidisciplinary projects
{{% /greyed %}}

{{% greyed %}}

##### SSA & ProCirque

grant for contemporary circus authors
{{% /greyed %}}

{{% greyed %}}

##### The CircusDanceFestival

residency program
Funded by TANZPAKT Stadt-Land-Bund with the support of the German Federal  Government Commissioner for Culture and the Media – production grant
{{% /greyed %}}
{{% /section %}}