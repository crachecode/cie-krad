---
title: kraD manifesto
draft: false
description: kraD manifesto
menu: main
weight: 2
image: /img/krad-manifesto.jpg
---
## kraD manifesto

Ô darkness  

Art noir, déversoir des abysses de nos natures\
Obscures, art miroir ;\
Reflet viscéral des méandres de nos pensées,\
Plongée dans nos entrailles\
Nous hurlons à qui veut entendre,\
Nous murmurons dans le néant\
Nous sculptons l’expression avec nos propres règles     

Extrême catharsis\
Poésie noire   

Art défouloir, réverbérant souffrances et angoisses\
Art exutoire, vomissant le trop-plein\
Art purgatoire, invoquant et célébrant nos démons\
Leurs traces sont éclatantes, laissant des cicatrices.\
Ainsi nous sommes\
Gueulardes, bruyantes et fracassantes,\
Stridentes, tapageuses et vibrantes\
Tout se reflète, tout résonne ;\
Son et mouvement, connexion et expression\
Notre compás   

Ô darkness     
Energie spiralante  
Tout est lié     

Cercles vicieux ou vertueux,\
Se répétant, se ressassant    

Besoin vital d'exprimer le non-exprimable\
Hantées par les tumultes de la pensée,\
Obsédées d’éternelles questions\
Sans fin, sans réponses   

Nous sommes agitées\
Par le désir de créer   

Pour tous les névrosés, tarés, maniaques et démoniaques, tox et borderline