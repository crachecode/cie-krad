---
title: Contact
draft: false
description: kraD contacts
menu: main
weight: 5
---
## The Cie *kraD* is<br>based in Geneva

cie@krad.xyz

#### F﻿riends

Graphisme Clément Coubès coubes.clement@gmail.com\
Développement web Timothée Crozat [crachecode.net](https://www.crachecode.net/)\
Réalisation teaser Éric Desjeux [ericdesjeux.com](https://www.ericdesjeux.com/)\
Costumes Irène Schlatter

#### C﻿onnection

Saules d'Out\
cave12 [cave12.org](https://www.cave12.org/)\
Station Circus [stationcircus.ch](https://www.stationcircus.ch/)\
Dents de Scie [dents-de-scie.blogspot.com](https://dents-de-scie.blogspot.com/)

#### P﻿hoto Crédits

Alison Johnson\
Rose Fayet\
Felipe la Roca\
Éric Desjeux\
Neige Sanchez\
Agnès Voulon    
Dorota Grajewska