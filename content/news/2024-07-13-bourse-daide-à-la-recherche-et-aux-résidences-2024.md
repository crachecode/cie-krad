---
title: Research and residency grant 2024
draft: false
date: 2024-07-03T06:41:21.641Z
place: Geneva
description: The Cie kraD was awarded with the research and residency grant 2024
  by the City of Geneva for its second project  "Son_brrr"
image: /img/photo_2024-07-19_14-04-08-logo.png
admin_show: true
---
In this second artistic research, the company pursues its quest to fuse two artistic domains - noise and vertical rope - to produce a hybrid, minimal play, thus establishing this new approach to physical performance.