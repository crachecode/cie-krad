---
title: Arachnur crawls of to Köln
draft: false
date: 2020-09-12T20:19:15.434Z
place: Köln, Germany
description: "One-week creation residency with Arachnur in Cologne, Germany, "
image: /img/celle-là-pour-sur-logo.png
link: https://www.circus-dance-festival.de/
admin_show: true
---
One-week creation residency with Arachnur in Cologne, Germany, and obtention of a production grant from the “CircusDanceFestival residency program – Funded by TANZPAKT Stadt-Land-Bund with the support of the German Federal Government Commissioner for Culture and Media” 