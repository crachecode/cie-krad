---
title: Cie kraD is back for a research residency
draft: false
date: 2024-11-04T14:50:00.692Z
place: Ateliers BH, Geneva
image: /img/photo_2024-11-05_15-47-51.jpg
admin_show: true
---
In their first research residency of the project "Son_brrr", the Cie kraD delves deeper into creating sound-mouvements through vertical rope and dance.  