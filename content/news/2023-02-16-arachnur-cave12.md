---
title: ABSOLUTE VISCERAL DARK_BEAST ARACHNUR ROPE & NOISE « NEW » PERFORMANCE
  ACT & TAPE VERNISSAGE
draft: false
date: 2023-02-16T21:00:26.695Z
place: cave12
description: Arachnur will perform live at cave12, Geneva.
image: /img/cave12-arachnur.jpg
link: https://www.cave12.org/arachnur-passage-de-disques/
admin_show: true
publishdate: 2021-02-16T20:00:26.695Z
---
portes: 21h00 / performance: 21h30!

### ARACHNUR (CH)
Leïla Maillard: performance, corde lisse  
nur: son, electronics, voix, performance  
Kiod Bariteau: lumières  
Irene Schlatter: costumes  
Katrine Zingg: perruque

\+

### BAMBA TRISTE
Passage de disque: arach_noise set spécialement pensé pour l’occasion

- ABSOLUTE VISCERAL DARK_BEAST ARACHNUR ROPE & NOISE « NEW » PERFORMANCE ACT-

ARACHNUR (CH)
Leïla Maillard: performance, corde lisse
nur: son, electronics, voix, performance
Kiod Bariteau: lumières

Création née pendant le confinement pandémique de la Cie kraD genevoise (dont une résidence confinée à la cave12), ARACHNUR est la rencontre improbable mais furieusement englobante, aimantante, hypnotique et réussie entre musique noise et corde lisse résultant en une performance à l’intensité coup-de-poing, brisant allègrement les codes & frontières entre techniques issues du monde du cirque et de la manipulation sonore abrasive. 


Entité constituée de cordes et de câbles, ARACHNUR est une créature vivante et sonore, s’ébranlant lentement pour mieux avaler/ingurgiter ses 2 protagonistes en un flux_torrent bruitiste et spasmodique, comme une lente plongée_agonie le long d’un long tube digestif duquel on ne ressort pas et où toute tentative de résistance/cri/danse est futile. Viscéralement dark_noire, ARACHNUR est une Bête macabre ne laissant aucune échappatoire aux deux personnages l’habitant et ne laissant aucun répit aux spectateur.trice.x.s /auditeur.trice.x.s témoins du rituel_engloutissement se déroulant de manière irrémédiable sous leurs yeux & oreilles. 


Une performance forte, radicale et pensée au départ pour grand plateau/espace. Spécialement re_imaginée, re-pensée et re_mixée ce soir pour l’espace intimiste de la cave12 après 2 jours de préparation sur place, gageons que le résultat sera plus suffoquant que jamais et qu’ARACHNUR nous engluera complètement nous et la cave12 au sein de sa bile venimeuse intoxicante. Ou comment être au plus proche du Nid de la Bête (Noire) …et ne plus en ressortir… ? 


Totalement unique en son genre et moment fort en perspective. 


Rare et, dans le genre, recommandé. 

(Sixto) 




cave12 :  
Rue de la Prairie 4  
1202 Genève