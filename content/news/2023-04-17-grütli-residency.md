---
title: Arachnur work in progress in the Grütli
draft: false
date: 2021-04-04T20:01:45.988Z
place: Théâtre du Grütli
description: One week residency
image: /img/14-photo_2022-07-08-21.14.40.jpeg
link: https://grutli.ch/
admin_show: true
---
One-week creation residency with Arachnur at the Grütli, Geneva, working for the first time with our light technician Kiod. Small presentation at the end of the residency for a privat audience