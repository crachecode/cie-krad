---
title: Arachnur creeps into the cave12
draft: false
date: 2020-12-14T21:12:58.440Z
place: cave12
description: 3 day residency in the cave12
image: /img/photo_2024-07-13_08-25-56.jpg
link: https://www.cave12.org/
admin_show: true
---
3-day sound creation residency with Arachnur at cave12, Geneva