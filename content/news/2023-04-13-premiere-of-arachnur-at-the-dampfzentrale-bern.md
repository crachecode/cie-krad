---
title: Premiere of Arachnur at the Dampfzentrale Bern
draft: false
date: 2022-10-15T19:29:55.320Z
place: Dampfzentrale, Berne
description: "Arachnur's final residency at the Dampfzentrale in Bern, followed
  by the grand Premiere on October 15, 2022 "
image: /img/flyer-arachnur.jpg
link: https://www.dampfzentrale.ch/en/event/e-cie-krad_oktober-2022/
admin_show: true
---


The first creation of the Cie kraD, Leïla Maillard and nur, «Arachnur» shows a fusion of contemporary circus and experimental music with a strong preference for the obscure. «Arachnur» is beyond evil or good, definitely cathartic, yet poetic. A sustained hymn to the dark and what lies beyond, directly inspired by the quote of painter Pierre Soulages: «Mon instrument n’était plus le noir, mais cette lumière secrète venue du noir.» (My instrument was no longer the darkness, but the secret light from the darkness…).  

A deep dive into the abyss of the human soul through acoustic borderlines and dark imagery. A macabre ritual that embodies a spiritual crawl to the gallows via despair and the harrowing loop of dark thoughts.  

Leïla Maillard: Performance  
nur: Sound  
Kiod Bariteau: Light  
Irène Schlatter: Costume  
Kathrin Zingg: Makeup 
 

