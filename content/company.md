---
title: The company
draft: false
description: The kraD company
menu: main
weight: 3
layout: company
---
## Born from the encounter of two Swiss artists, Nur C. and Leïla Maillard, and founded in spring 2020, the Cie kraD is a multidisciplinary performance company,  merging contemporary circus and noise in a dark aesthetic.